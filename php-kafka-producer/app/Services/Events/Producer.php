<?php

declare(strict_types = 1);

namespace App\Services\Events;

interface Producer
{
    public function produce(string $message): void;
}
