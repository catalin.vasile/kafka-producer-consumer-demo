<?php

declare(strict_types=1);

namespace App\Services\Events\Impl;

use App\Services\Events\Producer;
use Psr\Log\LoggerInterface;
use RdKafka\Producer as RdKafkaProducer;

class KafkaProducer implements Producer
{
    private RdKafkaProducer $producer;
    private LoggerInterface $logger;
    private string $topic;
    private int $maxFlushRetries;
    private int $timeout;

    public function __construct(
        RdKafkaProducer $producer,
        LoggerInterface $logger,
        string $topic,
        int $maxFlushRetries,
        int $timeout
    ) {
        $this->producer = $producer;
        $this->logger = $logger;
        $this->topic = $topic;
        $this->maxFlushRetries = $maxFlushRetries;
        $this->timeout = $timeout;
    }

    public function produce(string $message): void
    {
        $this->producer->newTopic($this->topic)->producev(
            RD_KAFKA_PARTITION_UA,
            0,
            $message,
            null,
            [
                'X-DEMO-H' => 'TAKEAWAY'
            ]
        );

        $this->logger->info("Event published to Kafka. [topicName => $this->topic, 'message' => $message]");
    }

    public function __destruct()
    {
        for ($flushRetries = 0; $flushRetries < $this->maxFlushRetries; $flushRetries++) {
            $result = $this->producer->flush($this->timeout);
            if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
                return;
            }
        }

        $this->logger->error('Was unable to flush, messages might be lost!');
    }
}
