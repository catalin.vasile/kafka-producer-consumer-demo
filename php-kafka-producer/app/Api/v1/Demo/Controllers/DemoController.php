<?php

declare(strict_types = 1);

namespace App\Api\v1\Demo\Controllers;

use App\Api\v1\Demo\Requests\DemoInputRequest;
use App\Http\Controllers\Controller;
use App\Services\Events\Producer;
use Illuminate\Http\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends Controller
{
    public function produceJson(
        DemoInputRequest $request,
        LoggerInterface $logger,
        Producer $producer
    ): JsonResponse {
        $logger->info("Input data: [email => " . $request->getEmail() . ", name => ". $request->getName() . "]");

        $producer->produce("{\"email\":\"{$request->getEmail()}\", \"name\":\"{$request->getName()}\"}");

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
