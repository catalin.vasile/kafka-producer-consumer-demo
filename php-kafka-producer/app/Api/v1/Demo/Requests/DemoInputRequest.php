<?php

declare(strict_types = 1);

namespace App\Api\v1\Demo\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DemoInputRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'name' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => 'Field required.',
            'email.email' => 'Invalid email.',
            'name.required' => 'Field required.'
        ];
    }

    public function getEmail(): string
    {
        return $this->get('email');
    }

    public function getName(): string
    {
        return $this->get('name');
    }
}
