## Intro to Kafka 
### Producer/Consumer API Demo
To bootstrap the APIs, including a local Kafka broker with default configurations, run the following docker-compose command.  

```docker-compose -f docker-compose.php-kafka-producer.yml -f docker-compose.dotnet-kafka-consumer.yml -f docker-compose.php-kafka-consumer.php -f docker-compose.kafka-dev.yml up```

To produce a message using the Swagger UI, access `http://localhost:4000`in your browser and start using the Demo endpoint.

To visualise details such as topics with their related messages and partitions within the local Kafka broker, a UI web 
tool can be accessed at `http://localhost:9091` in your browser.
