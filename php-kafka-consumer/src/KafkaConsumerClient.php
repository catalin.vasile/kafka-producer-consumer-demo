<?php

declare(strict_types = 1);

namespace App;

use RdKafka\Conf;
use RdKafka\KafkaConsumer;

class KafkaConsumerClient
{
    private const KAFKA_TOPICS = ['com.demo.intro.kafka'];
    private KafkaConsumer $consumer;

    public function __construct(Conf $configuration)
    {
        $this->consumer = new KafkaConsumer($configuration);
        $this->consumer->subscribe(self::KAFKA_TOPICS);
    }

    public function startConsuming(): void
    {
        while (true) {
            $message = $this->consumer->consume(100);

            if (RD_KAFKA_RESP_ERR__PARTITION_EOF === $message->err) {
                echo 'Reached end of partition, waiting for more messages...' . PHP_EOL;
                continue;
            } else if (RD_KAFKA_RESP_ERR__TIMED_OUT === $message->err) {
//                echo 'Timed out without receiving a new message, waiting for more messages...' . PHP_EOL;
                continue;
            } else if (RD_KAFKA_RESP_ERR_NO_ERROR !== $message->err) {
                echo rd_kafka_err2str($message->err) . PHP_EOL;
                continue;
            }

            echo sprintf(
                    'Read message with key:%s payload:%s topic:%s partition:%d offset:%d',
                    $message->key,
                    $message->payload,
                    $message->topic_name,
                    $message->partition,
                    $message->offset
                ) . PHP_EOL;

            $this->consumer->commit($message);
        }
    }
}
