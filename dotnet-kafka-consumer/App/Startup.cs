namespace KafkaConsumer
{
    using Confluent.Kafka;
    using KafkaConsumer.Services;
    using KafkaConsumer.Services.Impl;
    using Options;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;
    
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();

            services.Configure<ConsumerConfiguration>(
                this.Configuration.GetSection(nameof(ConsumerConfiguration)));
            
            services.AddSingleton<Consumer, KafkaConsumerClient>();
            
            services.AddSingleton<IConsumer<Ignore, string>>(
                sp => new ConsumerBuilder<Ignore, string>(sp
                    .GetRequiredService<IOptions<ConsumerConfiguration>>()
                    .Value
                    .Configuration).Build());

            services.AddHostedService<KafkaService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health-check");
                endpoints.MapGet(
                    "/{**path}",
                    async context => await context.Response.WriteAsync("Navigate to /health-check to see the health status.").ConfigureAwait(false));
            });
        }
    }
}
