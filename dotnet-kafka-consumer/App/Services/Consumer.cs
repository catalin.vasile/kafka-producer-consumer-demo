namespace KafkaConsumer.Services
{
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface Consumer
    {
        public void Consume(CancellationToken ct);
    }
}
