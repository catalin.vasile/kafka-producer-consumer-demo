namespace KafkaConsumer.Services.Impl
{
    using System.Threading;
    using System.Threading.Tasks;
    using KafkaConsumer.Options;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    
    public class KafkaService : BackgroundService
    {
        private readonly ILogger<KafkaService> logger;
        private readonly Consumer consumer;
        private readonly ConsumerConfiguration consumerConfiguration;

        public KafkaService(
            ILogger<KafkaService> logger,
            IOptions<ConsumerConfiguration> consumerConfiguration,
            Consumer consumer)
        {
            this.logger = logger;
            this.consumerConfiguration = consumerConfiguration.Value;
            this.consumer = consumer;
        }
        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            this.logger.LogInformation(
                "Starting Background Worker Service dotnet-kafka-consumer." 
                + "\nConsuming the following topics: [{topics}]",
                string.Join(", ", this.consumerConfiguration.Topics));

            while (!stoppingToken.IsCancellationRequested)
            {
                this.consumer.Consume(stoppingToken);
            }

            return Task.CompletedTask;
        }
    }
}
