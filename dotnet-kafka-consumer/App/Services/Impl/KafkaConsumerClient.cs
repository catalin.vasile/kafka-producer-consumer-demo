namespace KafkaConsumer.Services.Impl
{
    using System;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Confluent.Kafka;
    using KafkaConsumer.Options;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    
    public class KafkaConsumerClient : Consumer, IDisposable
    {
        private const string KafkaMessageHeader = "X-DEMO-H";
        private readonly ILogger<KafkaConsumerClient> logger;
        private readonly ConsumerConfiguration consumerConfiguration;
        private readonly IConsumer<Ignore, string> consumer;

        public KafkaConsumerClient(
            ILogger<KafkaConsumerClient> logger,
            IOptions<ConsumerConfiguration> consumerConfiguration,
            IConsumer<Ignore, string> consumer)
        {
            this.logger = logger;
            this.consumerConfiguration = consumerConfiguration.Value;
            this.consumer = consumer;
            this.consumer.Subscribe(this.consumerConfiguration.Topics);
        }

        public void Consume(CancellationToken ct)
        {
            var result = this.GetConsumeResult();

            if (result == null || result.IsPartitionEOF || result.Message == null) return;
            
            this.logger.LogInformation(
                "About to consume the following message: " + 
                "[message => {message}, header => {messageHeader}={header}]", 
                result.Message.Value,
                KafkaMessageHeader,
                Encoding.Default.GetString(result.Message.Headers.GetLastBytes(KafkaMessageHeader)));
                
            this.consumer.Commit(result);
        }
        
        private ConsumeResult<Ignore, string> GetConsumeResult()
        {
            return this.consumer.Consume(100);
        }
        
        public void Dispose()
        {
            this.consumer?.Close();
            this.consumer?.Dispose();
        }
    }
}
