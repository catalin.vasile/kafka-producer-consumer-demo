namespace KafkaConsumer
{
    using System;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;
    using Serilog.Sinks.SystemConsole.Themes;
    
    public static class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .AddLoggerConfiguration("App", Environment.GetEnvironmentVariable("LOG_LEVEL"))
                .CreateLogger();

            try
            {
                Log.Information("Initializing service");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception exception)
            {
                Log.Fatal(exception, "Service unexpectedly terminated");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) => config.AddEnvironmentVariables(prefix: "Demo_"))
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>())
                .UseSerilog();

        private static LoggerConfiguration AddLoggerConfiguration(
            this LoggerConfiguration loggerConfiguration,
            string serviceNamespace = "<unknown>",
            string level = "Information")
        {
            var levelSwitch = new LoggingLevelSwitch
            {
                MinimumLevel = !string.IsNullOrEmpty(level) && Enum.TryParse<LogEventLevel>(level, true, out var value) ? value : LogEventLevel.Information,
            };

            var overrideLevel = levelSwitch.MinimumLevel == LogEventLevel.Information ? LogEventLevel.Warning : levelSwitch.MinimumLevel;

            return loggerConfiguration
                .MinimumLevel.ControlledBy(levelSwitch)
                .MinimumLevel.Override("Microsoft", overrideLevel)
                .MinimumLevel.Override("System", overrideLevel)
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate: $"[{{Timestamp:yyyy-MM-dd HH:mm:ss.fff}} {serviceNamespace} {{Level:u3}}] {{Message:j}}{{NewLine}}{{Exception}}",
                    theme: AnsiConsoleTheme.Code);
        }
    }
}
