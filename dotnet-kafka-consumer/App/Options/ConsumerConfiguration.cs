namespace KafkaConsumer.Options
{
    using System.Collections.Generic;
    using Confluent.Kafka;
    
    public class ConsumerConfiguration
    {
        public ConsumerConfig Configuration { get; set; }

        public IEnumerable<string> Topics { get; set; }
    }
}
